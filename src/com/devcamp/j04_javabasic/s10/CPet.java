package com.devcamp.j04_javabasic.s10;

public class CPet extends CAnimal {
    int age;//tuoi
    String name;//ten

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Pet sound!");
    }    

    public void eat() {
        System.out.println("Pet eating...");
    }; 

    public void print() {
        System.out.println("Pet print!");
    }

    public void play() {
        System.out.println("Pet play!");
    }
    public static void main(String[] args) {
        CPet pet = new CPet();
        pet.animclass = AnimalClass.birds;
        pet.age = 3;
        pet.name = "Lulu";
        pet.animalSound();
        pet.eat();
        pet.play();
        pet.print();

        pet = new CFish();
        pet.animalSound();
        pet.eat();
        pet.play();
        pet.print();
        ((CFish)pet).swim();

        pet = new CBird();
        pet.animalSound();
        pet.eat();
        pet.play();
        pet.print();

        CFish fish = new CFish();
        fish.age = 2;
        fish.name = "Gold fish";
        fish.animclass = AnimalClass.fish;
        fish.animalSound();
        fish.eat();
        fish.play();
        fish.print();
        fish.swim();

        
    }
}
